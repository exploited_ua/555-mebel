ymaps.ready(function () {
    var myMap = new ymaps.Map('map', {
            center: [42.856977,74.617639],
            zoom: 16
        }, {
            searchControlProvider: 'yandex#search'
        }),
        myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
            hintContent: '',
            balloonContent: ''
        }, {
            // Опции.
            // Необходимо указать данный тип макета.
            iconLayout: 'default#image',
            // Своё изображение иконки метки.
            iconImageHref: './img/icon_map.png',
            // Размеры метки.
            iconImageSize: [87, 110],
            // Смещение левого верхнего угла иконки относительно
            // её "ножки" (точки привязки).
            iconImageOffset: [-43, -110]
        });

    myMap.geoObjects.add(myPlacemark);
});
